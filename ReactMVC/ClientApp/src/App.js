import React, { Component } from 'react';
import { Route } from 'react-router';
import { Layout } from './components/Layout';
import { Home } from './components/Home';
import { FetchData } from './components/FetchData';
import { Counter } from './components/Counter';
import { TestData } from './components/TestData';
import { FormTest } from './components/FormTest';
import { GetDataApi } from './components/GetDataApi';
import { UserForm } from './components/UserForm';

export default class App extends Component {
    displayName = App.name

    render() {
        return (
            <Layout>
                <Route exact path='/' component={Home} />
                <Route path='/counter' component={Counter} />
                <Route path='/fetchdata' component={FetchData} />
                <Route path='/testdata' component={TestData} />
                <Route path='/formtest' component={FormTest} />
                <Route path='/users' component={GetDataApi} />
                <Route path='/create-user' component={UserForm} />
            </Layout>
        );
    }
}
