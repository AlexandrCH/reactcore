export function CreateUserPost(data) {
    return fetch('https://apialex.azurewebsites.net/api/users', {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json'
        }
    }).then(res => {
        console.log(res);
        return res;
    }).then(x => console.log(x.status)).catch(err => alert(err));
}