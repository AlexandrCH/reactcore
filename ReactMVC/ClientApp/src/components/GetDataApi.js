﻿import React, { Component } from 'react';
import { Table } from 'reactstrap';

export class GetDataApi extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            users: []
        };
    }

    componentDidMount() {
        fetch('https://apialex.azurewebsites.net/api/all')
            .then(response => response.json())
            .then(data => this.setState({ users: data }));
    }
    render() {
        const { users } = this.state;
        return (

            <div>
                <h1>All users from https://apialex.azurewebsites.net/api/all</h1>
                <div>
                    <Table hover>
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Surname</th>
                                <th>Email</th>
                                <th>Age</th>
                            </tr>
                        </thead>
                        <tbody>
                            {users.map(user => (
                                <tr key={user.Id}>
                                    <td>{user.Id}</td>
                                    <td>{user.Name}</td>
                                    <td>{user.Surname}</td>
                                    <td>{user.Email}</td>
                                    <td>{user.Age}</td>
                                </tr>
                            ))}
                        </tbody>
                    </Table>
                </div>
            </div>
        );
    }
}
