﻿import { CreateUserPost } from './apiPOST';
import React from 'react';
import { Button, Form, FormGroup, Label, Input, Col } from 'reactstrap';
export class UserForm extends React.Component {
    constructor(props) {
        super(props);
        this.state = { name: "", email: "", surname: "", age: 0 };

        this.onNameChange1 = this.onNameChange1.bind(this);
        this.onEmailChange = this.onEmailChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }
    onNameChange1(e) {
        var val = e.target.value;
        this.setState({ name: val });
    }
    onEmailChange(e) {
        var val = e.target.value;
        this.setState({ email: val });
    }

    onSurnameChange(e) {
        var val = e.target.value;
        this.setState({ surname: val });
    }

    onAgeChange(e) {
        var val = e.target.value;
        this.setState({ age: val });
    }

    handleSubmit(e) {
        e.preventDefault();
        var user = {
            "Id": 151,
            "Name": this.state.name,
            "Surname": "ddddddddd",
            "Email": this.state.email,
            "Age": 311
        };

        CreateUserPost(user);
         alert("Name: " + this.state.name);
    }

    render() {
        return (

            // <form onSubmit={this.handleSubmit}>
            //     <p>
            //         <label>Имя:</label><br />
            //         <input type="text" value={this.state.name} onChange={this.onNameChange1}/>
            //     </p>
            //     <p>
            //         <label>Имя:</label><br />
            //         <input type="text" value={this.state.email} onChange={this.onEmailChange}/>
            //     </p>
            //     <input type="submit" value="Отправить" />
            // </form>


            <Form onSubmit={this.handleSubmit}>
                <FormGroup row>
                    <Label for="userName" sm={2}>User Name</Label>
                    <Col sm={10}>
                        <Input type="text" name="name" id="name" placeholder="User Name" value={this.state.name} onChange={this.onNameChange1} />
                    </Col>
                </FormGroup>
                <FormGroup row>
                    <Label for="email" sm={2}>Email</Label>
                    <Col sm={10}>
                        <Input type="email" name="email" id="email" placeholder="email" value={this.state.email} onChange={this.onEmailChange} />
                    </Col>
                </FormGroup>
                <Button type="submit">Submit</Button>
            </Form>
        );
    }
}
