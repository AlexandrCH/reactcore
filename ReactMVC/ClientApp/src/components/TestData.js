﻿import React, { Component } from 'react';

export class TestData extends Component {
    constructor(props) {
        super(props);
        this.state = { testData: [], loading: true };

        fetch('api/SampleData/WeatherForecasts')
            .then(response => response.json())
            .then(data => {
                this.setState({ testData: data, loading: false });
            });
    }

    render() {
        return (
            <div>
                <h1>Test list from api</h1>
                <table className='table'>
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Temp. (C)</th>
                            <th>Temp. (F)</th>
                            <th>Summary</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.testData.map(x =>
                            <tr key={x.dateFormatted}>
                                <td>{x.dateFormatted}</td>
                                <td>{x.temperatureC}</td>
                                <td>{x.temperatureF}</td>
                                <td>{x.summary}</td>
                            </tr>
                        )}
                    </tbody>
                </table>

            </div>
        );
    }
}